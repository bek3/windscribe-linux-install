#!/usr/bin/env bash

wget https://repo.windscribe.com/fedora/windscribe.repo -O /etc/yum.repos.d/windscribe.repo
yum update
yum install windscribe-cli
