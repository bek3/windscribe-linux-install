#!/usr/bin/env bash

wget https://repo.windscribe.com/centos/windscribe.repo -O /etc/yum.repos.d/windscribe.repo
yum update
yum install epel-release
yum install windscribe-cli
